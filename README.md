Title: FastNet: Fast and accurate inference of
phylogenetic networks using large-scale genomic
sequence data

Authors: Hussein A. Hejase & Natalie VandePol & Gregory A. Bonito & Kevin J. Liu

LICENSE: All source code are distributed under the terms of the GNU General Public License as published by the Free Software Foundation.
You can distribute or modify source code under the terms of the GNU General Public License either version 3 of the License
(https://www.gnu.org/licenses/gpl-3.0.txt) or any later version.
All data are distributed under the terms of the Creative Commons Attribution-ShareAlike 4.0 International license
(https://creativecommons.org/licenses/by-sa/4.0/).


The FastNet folder contains the simulated and empirical datasets and the scripts used to run the analysis:

————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————


The data/scripts used in this study are located in the following folders:

1- inferred-non-deep-model-conditions: contains true genetrees and sequence alignments used to infer a genetree using non-deep gene flow model conditions. This folder also contains folders named \<number of taxa\>-\<number of reticulations\>. For example; 15-1 folder contains genetrees that were used to infer a network with 15 taxa and a single reticulation. We also add genetrees used by each method (i.e. mle or mle-length or mpl). model-networks.txt is a 20-line file containing the model phylogeny for each replicate (one line per replicate).

2- inferred-vary-loci: contains true genetrees and sequence alignments used to infer a genetree using non-deep gene flow model conditions where we vary the number of loci from 100 to 1000. model-networks.txt is a 20-line file containing the model phylogeny for each replicate (one line per replicate).

3- true-deep-model-conditions: contains true genetrees using deep gene flow model conditions. model-networks.txt is a 20-line file containing the model phylogeny for each replicate (one line per replicate).

4- true-non-deep-model-conditions: contains true genetrees using non-deep gene flow model conditions (i.e. 30-4/mpl contains the genetrees used to infer a species network containing 30 taxa and four reticulations using MPL as a base method). model-networks.txt is a 20-line file containing the model phylogeny for each replicate (one line per replicate).

5- mosquito-dataset: contains inferred genetrees used in the mosquito analysis.

6- yeast-dataset: contains inferred genetrees used in the yeast analysis.

7- scripts: contains scripts used by FastNet to infer a species network. run.sh is a wrapper script that controls the execution of the other scripts. The parameters used by FastNet can be changed in this master script.

