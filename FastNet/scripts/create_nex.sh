path=$1
numSpecies=$2
ret=$3
genetrees=$4

for file in $path/$numSpecies/genetrees/size_subproblem_*.txt;
do
  f="${file%.*}"
  IFS='_' read -r -a array_par <<< "$f"

  n=$(cat $file | wc -l)
  for k in `seq 1 $n`;
  do
    sub=$path/$numSpecies/genetrees/subproblem'_'${array_par[2]}'_'${array_par[3]}'_'${array_par[4]}'_'${array_par[5]}'_'${array_par[6]}'_'$k.txt
    echo $sub
    i=$(sed -n $k'p' < $file)
    echo $i
    if [ $i -eq 1 -o $i -eq 2 ]; then
      echo "Don't run anything"
    elif [ $i -eq 3 ]; then
      echo "Run MPL - default mode"
      for r in `seq 0 $ret`;
      do
        sh convert_MPL.sh $sub $r $genetrees default
      done
    elif [ $i -gt 12 ]; then
      echo "Run MPL"
      for r in `seq 0 $ret`;
      do
        sh convert_MPL.sh $sub $r $genetrees
      done
    elif [ $i -gt 8 ]; then
      echo "Run MPL - 1000 network searches"
      for r in `seq 0 $ret`;
      do
        sh convert_MPL.sh $sub $r $genetrees 1000
      done
    else
      echo "Run MPL - default mode"
      for r in `seq 0 $ret`;
      do
        sh convert_MPL.sh $sub $r $genetrees default
      done
    fi
  done
done
