path=$1
ret=$2
type=$3

currdir=$(pwd)
cd $path

for file in net'_'$type'_'*'_'$ret;
do
  echo $file
  echo "#NEXUS" > temp.nex
  echo "BEGIN NETWORKS;" >> temp.nex
  echo "Network net = " >> temp.nex
  cat $file >> temp.nex
  echo "END;" >> temp.nex

  echo "" >> temp.nex

  echo "BEGIN PHYLONET;" >> temp.nex
  echo "Charnet net -m tree;" >> temp.nex
  echo "END;" >> temp.nex

  mv temp.nex $file.tree

  java -jar /Users/hijazihu/PhyloNet_3.6.1.jar $file.tree | grep '(' | wc -l > $file.tree.num
done

cd $currdir
