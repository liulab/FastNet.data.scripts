path=$1
numSpecies=$2
height=$3
migration=$4
theta=$5
numRep=$6

for i in `seq 1 $numRep`;
do
  fname=$path/$numSpecies/gt'_'with'_'outgroup'_'$numSpecies'_'$height'_'$migration'_'$theta'_'$i.txt
  java -jar /Users/hijazihu/ASTRAL-4.7.6/Astral/astral.4.7.6.jar -i $fname -o $fname.output
done
