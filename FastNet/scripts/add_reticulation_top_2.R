library(ape)

#args <- commandArgs(TRUE)

path <- "/Users/hijazihu/Desktop/research/vary-taxa-ret/mpl" #args[1]
numRep <- 30 #as.numeric(args[2])
numRet <- 2 #as.numeric(args[3])
taxa <- 21 #as.numeric(args[4])
height <- 5 #as.numeric(args[5])
migration <- 5 #as.numeric(args[6])
theta <- 0.08 #as.numeric(args[7])
numgenetrees <- 1000 #as.numeric(args[8])
method <- "InferNetwork_MPL" #args[9]
iteration <- 1 #as.numeric(args[10])

is_present <- function(net){
  pattern1 <- gregexpr(paste("(",taxa,")#H1",sep=""),net,fixed = TRUE)
  pattern2 <- gregexpr(paste("(#H1,",taxa,")",sep=""),net,fixed = TRUE)
  pattern3 <- gregexpr(paste("(",taxa,",#H1)",sep=""),net,fixed = TRUE)
  
  if(pattern1[[1]][1]!=-1 || pattern2[[1]][1]!=-1 || pattern3[[1]][1]!=-1){
    return(TRUE)
  }
  return(FALSE)
}

drop_outgroup <- function(net){
  net <- toString(net)
  curr <- 0
  open <- c()
  close <- c()
  pos <- c()
  text <- unlist(strsplit(net, NULL))
  for(i in 1:length(text)){
    if(text[i]=="("){
      open <- c(open, i)
      curr <- curr + 1
    }
    
    if(text[i]==")"){
      close <- i
      pos <- rbind(pos, c(tail(open,n=1),close))
      open <- open[-(length(open))]
      curr <- curr - 1
    }
  }
  
  pattern1 <- gregexpr(paste("(",taxa,",",sep=""),net,fixed = TRUE)
  pattern2 <- gregexpr(paste(",",taxa,")",sep=""),net,fixed = TRUE)
  curr <- -1
  
  if(pattern1[[1]][1]!=-1){
    curr <- pattern1[[1]][1]
    ind <- pos[which(pos[,1]==curr),2]
    text[ind] <- ""
    for(j in curr:(curr+attr(pattern1[[1]],"match.length")-1)){
      text[j] <- ""
    }
    net <- paste(text,collapse = "")
  }
  
  if(pattern2[[1]][1]!=-1){
    curr <- pattern2[[1]][1]
    ind <- pos[which(pos[,2]==curr+attr(pattern2[[1]],"match.length")-1),1]
    text[ind] <- ""
    for(j in curr:(curr+attr(pattern2[[1]],"match.length")-1)){
      text[j] <- ""
    }
    net <- paste(text,collapse = "")
  }
  
  return(net)
}

get_subtrees <- function(s, t){
  curr <- 0
  open <- c()
  close <- c()
  pos <- c()
  text <- unlist(strsplit(s, NULL))
  for(i in 1:length(text)){
    if(text[i]=="("){
      open <- c(open, i)
      curr <- curr + 1
    }
    
    if(text[i]==")"){
      close <- i
      pos <- rbind(pos, c(tail(open,n=1),close))
      open <- open[-(length(open))]
      curr <- curr - 1
    }
  }
  
  all_subtrees <- c()
  for(i in 1:dim(pos)[1]){
    all_subtrees <- c(all_subtrees, paste(text[pos[i,1]:pos[i,2]], collapse = ""))
  }
  
  for(i in t){
    all_subtrees <- c(all_subtrees, i)
  }
  
  return(all_subtrees)
}

CalProb1 <- function(s, subproblem, candidate, t1, t2, taxa, replicate, n, size_subproblems, col1, col2, numOcc1, numOcc2, r){
  all_subtrees1 <- get_subtrees(subproblem,t1)
  all_subtrees2 <- get_subtrees(candidate,t2)
  labels <- union(t1,t2)
  join <- intersect(t1, t2)
  all_subtrees2 <- setdiff(all_subtrees2, join)
  
  edges <- read.table(paste(path, "/", taxa, "/genetrees/top", iteration, "/top_", replicate, "_", n, "_", col1, "_", col2, "_", numOcc1, "_", numOcc2, "_", r, ".txt", sep=""), header = FALSE)
  prob <- read.table(paste(path, "/", taxa, "/genetrees/top", iteration, "/res_prob_top_", replicate, "_", n, "_", col1, "_", col2, "_", numOcc1, "_", numOcc2, "_", r, ".nex.output", sep=""), header = FALSE)
  prob <- prob[[1]]
  index <- which(prob==max(prob))
  if(length(index)>1){
    index <- index[1]
  }
  prob <- max(prob)
  all_subtrees2 <- toString(edges[index,2])
  
  #add reticulations to s
  f <- c()
  for(i in 1:length(all_subtrees1)){
    flag <- TRUE
    a1 <- gregexpr(paste("(",join,",",sep=""), all_subtrees2, fixed = TRUE)    
    a2 <- gregexpr(paste(",", join,")",sep=""), all_subtrees2, fixed = TRUE)
    
    if(a1[[1]][1]!=-1 || a2[[1]][1]!=-1){
      flag <- FALSE
    }
    
    if(flag==TRUE){
      f <- rbind(f, c(all_subtrees2, all_subtrees1[i]))
    }
  }
  
  edges <- c()
  net <- c()
  for(i in 1:dim(f)[1]){
    from <- f[i,1]
    to <- f[i,2]
    temp <- s
    
    if(is.element("(",unlist(strsplit(from, NULL)))==FALSE){
      temp <- gsub(paste("(", from, ")",sep=""),paste("((", from, ")#H",numOcc1 + numOcc2 + r,")",sep=""),temp,fixed=TRUE)
      temp <- gsub(paste("(", from, ",",sep=""),paste("((", from, ")#H",numOcc1 + numOcc2 + r,",",sep=""),temp,fixed=TRUE)
      temp <- gsub(paste(",", from, ")",sep=""),paste(",(", from, ")#H",numOcc1 + numOcc2 + r,")",sep=""),temp,fixed=TRUE)
      temp <- gsub(paste(",", from, ",",sep=""),paste(",(", from, ")#H",numOcc1 + numOcc2 + r,",",sep=""),temp,fixed=TRUE)
    } else {
      temp <- gsub(from,paste("(",from,")#H",numOcc1 + numOcc2 + r,"",sep=""),temp,fixed=TRUE)
    }
    
    if(is.element("(",unlist(strsplit(to, NULL)))==FALSE){
      temp <- gsub(paste("(", to, ")",sep=""),paste("((", to, ",#H",numOcc1 + numOcc2 + r,"))",sep=""),temp,fixed=TRUE)
      temp <- gsub(paste("(", to, ",",sep=""),paste("((", to, ",#H",numOcc1 + numOcc2 + r,"),",sep=""),temp,fixed=TRUE)
      temp <- gsub(paste(",", to, ")",sep=""),paste(",(", to, ",#H",numOcc1 + numOcc2 + r,"))",sep=""),temp,fixed=TRUE)
      temp <- gsub(paste(",", to, ",",sep=""),paste(",(", to, ",#H",numOcc1 + numOcc2 + r,"),",sep=""),temp,fixed=TRUE)
    } else {
      temp <- gsub(to,paste("(",to,",#H",numOcc1 + numOcc2 + r,")",sep=""),temp,fixed=TRUE)
    }
    
    net <- c(net, temp)
    edges <- rbind(edges, cbind(from, to))
  }
  
  write.table(edges, paste(path, "/", taxa, "/genetrees/top_", replicate, "_", n, "_", col1, "_", col2, "_", numOcc1, "_", numOcc2, "_", r, ".txt", sep=""), col.names = FALSE, row.names = FALSE)
  
  genetrees <- read.tree(paste(path, "/", taxa, "/gt_with_outgroup_", taxa, "_", height, "_", migration, "_", theta, '_', replicate, ".txt", sep=""))
  drop <- setdiff(1:taxa, labels)
  for(i in 1:numgenetrees){
    genetrees[[i]] <- drop.tip(genetrees[[i]], as.character(drop))
  }
  
  sink(paste(path, "/", taxa, "/genetrees/top_", replicate, "_", n, "_", col1, "_", col2, "_", numOcc1, "_", numOcc2, "_", r, ".nex", sep=""), append=FALSE, split=FALSE)
  cat("#NEXUS")
  cat("\n\n")
  cat("BEGIN NETWORKS;")
  cat("\n")
  for(i in 1:length(net)){
    cat(paste("Network net", i, "=", sep=""))
    cat(net[i])
    cat("\n")
  }
  cat("END;")
  cat("\n\n")
  cat("BEGIN TREES;")
  cat("\n")
  for(n in 1:numgenetrees){
    cat(paste("Tree geneTree", n, "=", sep=""))
    cat("\n")
    cat(write.tree(genetrees[[n]]))
    cat("\n")
  }
  cat("END;")
  cat("\n\n")
  cat("BEGIN PHYLONET;")
  cat("\n")
  for(i in 1:length(net)){
    cat(paste(method, " (all) ", numOcc1 + numOcc2 + r, " -m 0 -md 0 -rd 0 -di -o -x 1 -s net", i, ";", sep=""))
    cat("\n")
  }
  cat("\n")
  cat("END;")
  sink()
}

for(replicate in 1:numRep){
  #Get subproblem networks
  name <- paste(path, "/", taxa, "/genetrees/size_subproblem_", taxa, "_", height, "_", migration, "_", theta, '_', replicate, ".txt", sep="")
  size_subproblems <- read.table(name, header = FALSE)
  size_subproblems <- size_subproblems[[1]]
  num_subproblems <- length(size_subproblems)
  
  subproblems_net <- matrix(0, num_subproblems, (iteration+1))
  
  index <- which(size_subproblems>=4)
  
  for(ret in 1:(iteration+1)){
    for(j in 1:num_subproblems){
      if(is.element(j,index)){
        file <- paste(path, "/", taxa, "/genetrees/net_subproblems_", replicate, "_", j, "_", ret-1, sep="")
        net <- read.delim(file, header = FALSE)
        net <- net[[1]]
        subproblems_net[j,ret] <- toString(net)
      } else {
        if(size_subproblems[j]==3){
          file <- paste(path, "/", taxa, "/genetrees/net_subproblems_", replicate, "_", j, "_", ret-1, sep="")
          net <- read.delim(file, header = FALSE)
          net <- net[[1]]
          if(is_present(net)==FALSE){
            net <- drop_outgroup(net)
            subproblems_net[j,ret] <- toString(net)
          } else {
            print("<<contains outgroup>>")
            subproblems_net[j,ret] <- subproblems_net[j,ret-1] 
          }
        } else if(size_subproblems[j]==2){
          name <- paste(path, "/", taxa, "/genetrees/subproblem_", taxa, "_", height, "_", migration, "_", theta, '_', replicate, "_", j,".txt",sep="")
          table <- read.tree(name)
          table[[1]]$edge.length <- NULL
          subproblems_net[j,ret] <- toString(write.tree(table[[1]]))
        } else if(size_subproblems[j]==1){
          name <- paste(path, "/", taxa, "/genetrees/subproblem_", taxa, "_", height, "_", migration, "_", theta, '_', replicate, "_", j,".txt",sep="")
          table <- read.table(name, header = FALSE)
          subproblems_net[j,ret] <- toString(table)
        }
      }
    }
  }
  
  #Get top structure probabilities and networks
  candidate_net <- matrix(1, 1, iteration+1)
  
  skip <- c()
  size_candidates <- read.table(paste(path, "/", taxa, "/genetrees/size_candidate_", replicate, "_", 1, ".txt", sep=""))
  size_candidates <- size_candidates[[1]]
  
  j <- 1
  if(size_candidates>2){
    for(ret in 1:(iteration+1)){
      file <- paste(path, "/", taxa, "/genetrees/net_candidate_", replicate, "_", j, "_", ret-1, sep="")
      candidate <- read.delim(file, header=FALSE)
      candidate <- candidate[[1]]
      
      if(size_candidates>3){
        candidate_net[j,ret] <- toString(candidate)
      } else {
        if(is_present(candidate)==FALSE){
          candidate <- drop_outgroup(candidate)
          candidate_net[j,ret] <- toString(candidate)
        } else {
          print("<<candidate contains outgroup>>")
          candidate_net[j,ret] <- candidate_net[j,ret-1]
          skip <- c(skip, ret)
        }
      }
      
      if(ret>1){
        file <- paste(path, "/", taxa, "/genetrees/net_candidate_", replicate, "_", j, "_", ret-1, '.tree.num', sep="")
        num <- read.delim(file, header=FALSE)
        num <- num[[1]]
        if(num<=iteration){     
          skip <- c(skip, ret)   
          candidate_net[ret] <- candidate_net[ret-1]
        }
      }
    }
  } else {
    file <- paste(path, "/", taxa, "/genetrees/candidate_", replicate, "_", j, ".txt", sep="")
    candidate <- read.delim(file, header=FALSE)
    candidate <- candidate[[1]]
    candidate <- read.tree(text = toString(candidate[1]))
    candidate <- drop.tip(candidate, as.character(taxa))
    for(ret in 1:(iteration+1)){
      candidate_net[j,ret] <- write.tree(candidate)
    }
  }
  
  print(replicate)
  print(subproblems_net)
  print(candidate_net)
  
  tree <- read.tree(text = candidate_net[,1])
  tips <- tree$tip.label
  
  for(j in 1:(iteration+1)){
    for(k in 1:(iteration+1)){
      if(is.element(k, skip)==FALSE){
        for(n in 1:length(size_subproblems)){
          if((j+k-2)<=iteration){
            if(size_subproblems[n]>1){
              if(is_present(subproblems_net[n,j])==FALSE && is_present(candidate_net[k])==FALSE){
                flag <- TRUE  
                
                if(k>1 && size_candidates>2){
                  file <- paste(path, "/", taxa, "/genetrees/net_candidate_", replicate, "_", 1, "_", k-1, '.tree.num', sep="")
                  num <- read.delim(file, header=FALSE)
                  num <- num[[1]]
                  if(num<=iteration){
                    flag <- FALSE 
                  }        
                }
                
                if(j>1 && size_subproblems[n]>2){
                  file <- paste(path, "/", taxa, "/genetrees/net_subproblems_", replicate, "_", n, "_", j-1, '.tree.num', sep="")
                  num <- read.delim(file, header=FALSE)
                  num <- num[[1]]
                  if(num<=iteration){
                    flag <- FALSE                   
                  }
                }
                
                if(j>1 && size_subproblems[n]==2){
                  flag <- FALSE
                }
                
                if(k>1 && size_candidates==2){
                  flag <- FALSE
                }
                
                if(flag==TRUE){
                  name <- paste(path, "/", taxa, "/genetrees/subproblem_", taxa, "_", height, "_", migration, "_", theta, '_', replicate, "_", n,".txt",sep="")
                  table <- read.tree(name)
                  table[[1]]$edge.length <- NULL
                  table <- table[[1]]$tip.label
                  table <- setdiff(table, taxa)
                  inter <- intersect(table, tips)      
                  labels <- union(table, tips)
                  
                  #need to replace subproblem with inter
                  t1 <- gregexpr(paste("(",inter,",",sep=""), candidate_net[k], fixed = TRUE)    
                  t2 <- gregexpr(paste(",",inter,")",sep=""), candidate_net[k], fixed = TRUE)
                  t4 <- gregexpr(paste("(",inter,")",sep=""), candidate_net[k], fixed = TRUE)
                  
                  if(t1[[1]][1]!=-1){
                    temp <- paste("(",inter,",",sep="")
                    t3 <- gsub(";", "", subproblems_net[n,j], fixed = TRUE)
                    temp <- gsub(temp, paste("(", t3, ",",sep=""), candidate_net[k], fixed=TRUE)        
                  }
                  
                  if(t2[[1]][1]!=-1){
                    temp <- paste(",",inter,")",sep="")
                    t3 <- gsub(";", "", subproblems_net[n,j], fixed = TRUE)
                    temp <- gsub(temp, paste(",", t3, ")", sep=""), candidate_net[k], fixed=TRUE)        
                  }
                  
                  if(t4[[1]][1]!=-1){
                    temp <- paste("(",inter,")",sep="")
                    t3 <- gsub(";", "", subproblems_net[n,j], fixed = TRUE)
                    temp <- gsub(temp, paste("(", t3, ")", sep=""), candidate_net[k], fixed=TRUE)        
                  }
                  
                  if((j+k-2)<iteration){
                    print(subproblems_net[n,j])
                    print(candidate_net[k])
                    CalProb1(temp, subproblems_net[n,j], candidate_net[k], table, tips, taxa, replicate, n, size_subproblems, j, k, j-1, k-1, iteration)                          
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}