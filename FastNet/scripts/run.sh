path="/Users/hijazihu/Desktop/research/vary-taxa-ret/ancestral/mpl"
taxa=21
height=5
migration=5
theta=0.08
numRep=30
subproblem_size=7
ret=1
genetrees=1000
method=InferNetwork_MPL

##############################
#Run ASTRAL to get guide tree#
##############################
sh run_ASTRAL.sh $path $taxa $height $migration $theta $numRep

##################
#Root ASTRAL tree#
##################
Rscript root_ASTRAL_tree.R $path $taxa $height $migration $theta $numRep

###################
#Infer subproblems#
###################
Rscript generate_subproblems.R $path $taxa $height $migration $theta $numRep $subproblem_size

mv $path/$taxa/size* $path/$taxa/genetrees
mv $path/$taxa/subprob* $path/$taxa/genetrees

###############################
#Create NEXUS files to run MLE#
###############################
sh create_nex.sh $path $taxa $ret $genetrees

#######################################################################
#Create datasets: for each dataset sample 1 taxon from each subproblem#
#######################################################################
sh run_ASTRAL_subproblems.sh $path/$taxa/genetrees
Rscript get_samples.R $path $taxa $height $migration $theta $numRep
sh run_candidate.sh $path $taxa $ret $genetrees cand

##############
#Run PhyloNet#
##############
#run run_PhyloNet.sh on an HPCC cluster

#####################################################
#Parse network scores for subproblems and candidates#
#####################################################
for i in `seq 0 $ret`;
do
  sh parse_network_subproblems.sh $path/$taxa/genetrees $i 
  Rscript select_network_Lscore_subproblems.R $path/$taxa/genetrees $numRep $i $taxa $height $migration $theta
done

for i in `seq 0 $ret`;
do
  sh parse_network_candidates.sh $path/$taxa/genetrees $i $numRep
  Rscript select_network_Lscore_candidate.R $path/$taxa/genetrees $numRep $i
done

###################################################################
#Get the number of induced trees in the subproblems and candidates#
###################################################################
for i in `seq 1 $ret`;
do
  sh charnet.sh $path/$taxa/genetrees $i candidate
  sh charnet.sh $path/$taxa/genetrees $i subproblems
done

#################################################################
#Search for reticulation edges between subproblems (iteration 1)#
#################################################################
Rscript add_reticulation_edge.R $path $numRep $ret $taxa $height $migration $theta $genetrees $method 1
Rscript add_reticulation_top.R $path $numRep $ret $taxa $height $migration $theta $genetrees $method 1
Rscript add_reticulation_top_2.R $path $numRep $ret $taxa $height $migration $theta $genetrees $method 1
#run run_PhyloNet.sh on an HPCC cluster

###################################################################
#Search for reticulation edges between subproblems (iteration 2-n)#
###################################################################
Rscript enumerate.R $path $numRep $ret $taxa $height $migration $theta $genetrees $method 2
Rscript enumerate_top.R $path $numRep $ret $taxa $height $migration $theta $genetrees $method 2
Rscript enumerate_top_2.R $path $numRep $ret $taxa $height $migration $theta $genetrees $method 2
#run run_PhyloNet.sh on an HPCC cluster
