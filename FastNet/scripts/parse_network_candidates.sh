path=$1
ret=$2
numRep=$3

currdir=$(pwd)
cd $path

for k in `seq 1 $numRep`;
do
  j=1
  s=$(cat size_candidate'_'$k'_'$j.txt)
  if [ $s -gt 2 ]; then
    iter=10
    for u in `seq 1 $iter`;
    do
      file=candidate'_'$k'_'$j.txt'_'$u'_'$ret.nex.output

      s=$(grep -A 3 'Inferred Network #1:' $file | tail -n 1)
      stringarray=($s)
      echo ${stringarray[4]} >> res'_'net'_'candidate'_'$k'_'$j'_'$ret

      s=$(grep -A 2 'Inferred Network #1:' $file | tail -n 1)
      stringarray=($s)
      echo ${stringarray[3]} >> res'_'prob'_'candidate'_'$k'_'$j'_'$ret
    done
    grep -v '^$' res'_'net'_'candidate'_'$k'_'$j'_'$ret > temp1.txt
    grep -v '^$' res'_'prob'_'candidate'_'$k'_'$j'_'$ret > temp2.txt
    mv temp1.txt res'_'net'_'candidate'_'$k'_'$j'_'$ret
    mv temp2.txt res'_'prob'_'candidate'_'$k'_'$j'_'$ret
  fi
done

cd $currdir
