path=$1
ret=$2
currdir=$(pwd)
cd $path

for file in size_subproblem_*;
do
  f="${file%.*}"
  IFS='_' read -r -a array_par <<< "$f"
  
  n=$(cat $file | wc -l)
  for k in `seq 1 $n`;
  do
    rep=${array_par[6]}

    i=$(sed -n $k'p' < $file)
    echo $i

    if [ $i -eq 1 ];
    then
      sub=subproblem'_'${array_par[2]}'_'${array_par[3]}'_'${array_par[4]}'_'${array_par[5]}'_'${array_par[6]}'_'$k.txt
    elif [ $i -eq 2 ];
    then
      sub=subproblem'_'${array_par[2]}'_'${array_par[3]}'_'${array_par[4]}'_'${array_par[5]}'_'${array_par[6]}'_'$k.txt
    else
      echo "Parse MPL"
      for iter in `seq 1 10`;
      do
        sub=subproblem'_'${array_par[2]}'_'${array_par[3]}'_'${array_par[4]}'_'${array_par[5]}'_'${array_par[6]}'_'$k.txt

        s=$(grep -A 3 'Inferred Network #1:' $sub'_'$iter'_'$ret.nex.output | tail -n 1)
        stringarray=($s)
        echo ${stringarray[4]} >> res'_'net'_'subproblems'_'$rep'_'$k'_'$ret

        s=$(grep -A 2 'Inferred Network #1:' $sub'_'$iter'_'$ret.nex.output | tail -n 1)
        stringarray=($s)
        echo ${stringarray[3]} >> res'_'prob'_'subproblems'_'$rep'_'$k'_'$ret
      done

      grep -v '^$' res'_'net'_'subproblems'_'$rep'_'$k'_'$ret > temp1.txt
      grep -v '^$' res'_'prob'_'subproblems'_'$rep'_'$k'_'$ret > temp2.txt
      mv temp1.txt res'_'net'_'subproblems'_'$rep'_'$k'_'$ret
      mv temp2.txt res'_'prob'_'subproblems'_'$rep'_'$k'_'$ret
    fi
  done
done

cd $currdir
