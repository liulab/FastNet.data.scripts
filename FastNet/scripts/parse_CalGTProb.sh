path=$1
cd $path
iteration=$2

for i in *$iteration.output;
do
  grep 'Total' $i | cut -d ' ' -f4 > res'_'prob'_'$i
done
