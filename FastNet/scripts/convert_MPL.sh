file=$1
ret=$2
genetrees=$3

echo "#NEXUS" > temp.nex
echo "BEGIN TREES;" >> temp.nex

for i in `seq 1 $genetrees `;
do
  #echo $i
  echo 'TREE gt'$i' = ' >> temp.nex

  if (($i == 1)); then
    tree=$tree'(gt'$i
  elif (($i == $genetrees)); then
    tree=$tree',gt'$i')'
  else
    tree=$tree',gt'$i
  fi

  sed -n $i'p' < $file >> temp.nex
done

echo "END;" >> temp.nex
echo "" >> temp.nex

echo "BEGIN PHYLONET;" >> temp.nex

echo 'InferNetwork_MPL (all) '$ret' -x 1 -di;' >> temp.nex

echo "END;" >> temp.nex

mv temp.nex $file'_'$ret.nex

for j in `seq 1 10`;
do
  cp $file'_'$ret.nex $file'_'$j'_'$ret.nex
done

rm $file'_'$ret.nex
