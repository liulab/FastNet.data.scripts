path=$1
taxa=$2
ret=$3
genetrees=$4
f=$5

for i in $path/$taxa/genetrees/$f*;
do

  s=$(echo $i | rev | cut -d"/" -f1 | rev)
  j=$(cat $path/$taxa/genetrees/size'_'$s)

  if [ $j -eq 1 -o $j -eq 2 ]; then
    echo "Don't run anything"
  elif [ $j -eq 3 ]; then
    echo "Run MPL"
    for r in `seq 0 $ret`;
    do
      sh convert_MPL.sh $i $r $genetrees default
    done
  elif [ $j -gt 12 ]; then
    echo "Run MPL"
    for r in `seq 0 $ret`;
    do
      sh convert_MPL.sh $i $r $genetrees
    done
  elif [ $j -gt 8 ]; then
    echo "Run MPL"
    for r in `seq 0 $ret`;
    do
      sh convert_MPL.sh $i $r $genetrees 1000
    done
  else
    echo "Run MPL"
    for r in `seq 0 $ret`;
    do
      sh convert_MPL.sh $i $r $genetrees default
    done
  fi

done
