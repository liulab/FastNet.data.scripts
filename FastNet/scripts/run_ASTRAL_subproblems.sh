path=$1

currdir=$(pwd)
cd $path

for file in size_subproblem_*.txt;
do

  f="${file%.*}"
  IFS='_' read -r -a array_par <<< "$f"
  
  n=$(cat $file | wc -l)
  for k in `seq 1 $n`;
  do
    sub=subproblem'_'with'_'root'_'${array_par[2]}'_'${array_par[3]}'_'${array_par[4]}'_'${array_par[5]}'_'${array_par[6]}'_'$k.txt

    i=$(sed -n $k'p' < $file)
    echo $i
    if [ $i -eq 1 -o $i -eq 2 ]; then
      echo "Don't run anything"
    else
      java -jar /Users/hijazihu/ASTRAL-4.7.6/Astral/astral.4.7.6.jar -i $sub -o ASTRAL'_'$sub.output.out
    fi
  done

done

cd $currdir
