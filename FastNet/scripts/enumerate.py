def multichoose(n,k):
    if k < 0 or n < 0: return "Error"
    if not k: return [[0]*n]
    if not n: return []
    if n == 1: return [[k]]
    return [[0]+val for val in multichoose(n-1,k)] + \
        [[val[0]+1]+val[1:] for val in multichoose(n,k-1)]

import  csv
k=1
for n in range(154,155):
  filename="out_"
  filename+=str(n)
  filename+='_'
  filename+=str(k)
  with open(filename,"w") as f:
    wr = csv.writer(f, delimiter=" ")
    wr.writerows(multichoose(n,k))

